from synapse.module_api import ModuleApi, DirectServeJsonResource
from synapse.types.state import StateFilter
from synapse.api.constants import EventTypes

from twisted.web.server import Request

from typing import Dict, Any
import attr
import logging
logger = logging.getLogger(__name__)


@attr.s(auto_attribs=True, frozen=True)
class TopSpacesConfig:
    pass


state_filter = StateFilter.from_types([
    (EventTypes.SpaceParent, None),
])


class TopSpacesServlet(DirectServeJsonResource):
    def __init__(self, api):
        self._api = api
        super().__init__()

    async def _async_render_GET(self, request: Request) -> str:
        await self._api._auth.get_user_by_req(request, allow_guest=False)

        handler = self._api._hs.get_room_list_handler()
        data = await handler.get_local_public_room_list()
        chunk = []
        for room in filter(lambda room: room.get('room_type') == 'm.space', data['chunk']):
            if await self.filter_child_spaces(room) and not await self._api._store.check_local_user_in_room(str(request.requester.user), room['room_id']):
                chunk.append(room)
        data['chunk'] = chunk
        data['total_room_count_estimate'] = len(data['chunk'])
        logger.debug('TopSpaces: returned %d results for %r', len(data['chunk']), request.requester)

        return 200, data

    async def filter_child_spaces(self, room):
        # return True if the space is not a child (has no parents)
        room_state = await self._api._storage_controllers.state.get_current_state(
            room_id=room['room_id'], state_filter=state_filter
        )
        return not bool(room_state)


class TopSpaces:
    def __init__(self, config: TopSpacesConfig, api: ModuleApi):
        # Keep a reference to the config and Module API
        self._api = api
        self._config = config

        self._api.register_web_resource(path='/_synapse/client/topUnjoinedSpaces',
                                        resource=TopSpacesServlet(api)
                                        )

    @staticmethod
    def parse_config(config: Dict[str, Any]) -> TopSpacesConfig:
        return TopSpacesConfig()
