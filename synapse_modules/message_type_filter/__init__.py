from synapse.module_api import ModuleApi
from synapse.config._base import ConfigError

from synapse.module_api import EventBase, NOT_SPAM, errors

from typing import Union, Dict, Any, List
import attr

import logging
logger = logging.getLogger(__name__)


@attr.s(auto_attribs=True, frozen=True)
class MessageTypeFilterConfig:
    blacklist: List[str] = []


class MessageTypeFilter:
    def __init__(self, config: MessageTypeFilterConfig, api: ModuleApi):
        # Keep a reference to the config and Module API
        self._api = api
        self._config = config

        self._api.register_spam_checker_callbacks(
            check_event_for_spam=self.message_type_filter_callback,
        )

    @staticmethod
    def parse_config(config: Dict[str, Any]) -> MessageTypeFilterConfig:
        blacklist = config.get('blacklist', [])
        if not isinstance(blacklist, (list, tuple)):
            raise ConfigError("Config option 'blacklist' must be a list")
        logger.info("MessageTypeFilter: Will block these message types: %r", blacklist)
        return MessageTypeFilterConfig(blacklist=blacklist)

    async def message_type_filter_callback(self, event: EventBase) -> Union[NOT_SPAM, errors.Codes]:
        try:
            if event.content['msgtype'] in self._config.blacklist:
                logger.info('MessageTypeFilter: blocked message type %r', event.content['msgtype'])
                return errors.Codes.FORBIDDEN
        except KeyError:
            pass
        return NOT_SPAM
