from synapse.module_api import ModuleApi
from synapse.config._base import ConfigError

from typing import Dict, Any, Optional
import attr

import logging
logger = logging.getLogger(__name__)


@attr.s(auto_attribs=True, frozen=True)
class AccountDataConfig:
    data: Dict[str, str] = {}


class StaticAccountData:
    def __init__(self, config: AccountDataConfig, api: ModuleApi):
        # Keep a reference to the config and Module API
        self._api = api
        self._config = config

        self._api.register_account_data_callbacks(
            on_account_data_updated=self.account_data_updated_callback,
        )

        self._api.register_account_validity_callbacks(
            on_user_registration=self.user_registration_callback,
        )

    @staticmethod
    def parse_config(config: Dict[str, Any]) -> AccountDataConfig:
        if not isinstance(config, list):
            raise ConfigError("Config be a list")
        for item in config:
            if not isinstance(item, dict):
                raise ConfigError("Every config item must be a dict.")
            if 'data_type' not in item or 'data' not in item:
                raise ConfigError("Every config item must have two items: 'data_type' and 'data'.")
            if not isinstance(item['data_type'], str):
                raise ConfigError(f"Config option 'data_type' mmust be str. Is {type(item['data_type'])} (value {item['data_type']!r}).")
        logger.info("StaticAccountData: Will set the account data: %r", config)
        return AccountDataConfig(data=config)

    async def account_data_updated_callback(self,
                                            user_id: str,
                                            room_id: Optional[str],
                                            account_data_type: str,
                                            content: "synapse.module_api.JsonDict") -> None:
        for block in self._config.data:
            data_type, data = block['data_type'], block['data']
            current_content = await self._api.account_data_manager.get_global(user_id=user_id, data_type=data_type)
            if current_content != data:
                logger.info('StaticAccountData AccountDataUpdated: Will set %r for %r.', data_type, user_id)
                ret = await self._api.account_data_manager.put_global(user_id=user_id,
                                                                      data_type=data_type,
                                                                      new_data=data)
                new_content = await self._api.account_data_manager.get_global(user_id=user_id, data_type=data_type)
                logger.info('StaticAccountData AccountDataUpdated: Done for user %r, exit code: %r. Result: %r', user_id, ret, new_content)
            else:
                logger.info('StaticAccountData AccountDataUpdated: Data of user %r is correct, type %r content %r', user_id, data_type, current_content)

    async def user_registration_callback(self, user_id: str) -> None:
        for block in self._config.data:
            data_type, data = block['data_type'], block['data']
            ret = await self._api.account_data_manager.put_global(user_id=user_id,
                                                                  data_type=data_type,
                                                                  new_data=data)
            new_content = await self._api.account_data_manager.get_global(user_id=user_id, data_type=data_type)
            logger.info('StaticAccountData Registration Callback: Done for user %r, exit code: %r. Result: %r', user_id, ret, new_content)
