from synapse.module_api import ModuleApi
from synapse.config._base import ConfigError
from synapse.types.state import StateFilter

from synapse.module_api import EventBase, NOT_SPAM, errors
from synapse.api.constants import EventTypes, AccountDataTypes, Membership

from typing import Union, Dict, Any, List, Optional
from itertools import chain
import attr

import logging
logger = logging.getLogger(__name__)


@attr.s(auto_attribs=True, frozen=True)
class RestrictDirectMessagesConfig:
    type_blacklist: List[str] = []
    maximum_members: Optional[int] = None


membership_state_filer = StateFilter.from_types([
    (EventTypes.Member, None),
])


class RestrictDirectMessages:
    def __init__(self, config: RestrictDirectMessagesConfig, api: ModuleApi):
        self._api = api
        self._config = config

        self._api.register_spam_checker_callbacks(
            check_event_for_spam=self.restrict_direct_message_types,
            user_may_invite=self.restrict_direct_message_invite,
        )

    @staticmethod
    def parse_config(config: Dict[str, Any]) -> RestrictDirectMessagesConfig:
        type_blacklist = config.get('type_blacklist', [])
        if not isinstance(type_blacklist, (list, tuple)):
            raise ConfigError("Config option 'type_blacklist' must be a list")
        logger.info("RestrictDirectMessages: Will block these message types in direct messages: %r", type_blacklist)

        maximum_members = config.get('maximum_members', [])
        if not isinstance(maximum_members, (int, None)) or maximum_members < 2:
            raise ConfigError("Config option 'maximum_members' must be an integer and >= 2.")
        logger.info("RestrictDirectMessages: Will restrict the number of members in direct messages to <= %d", maximum_members)

        return RestrictDirectMessagesConfig(type_blacklist=type_blacklist,
                                            maximum_members=maximum_members)

    async def restrict_direct_message_types(self, event: EventBase) -> Union[NOT_SPAM, errors.Codes]:
        # Retrieve user account data
        user_account_data = await self._api._store.get_global_account_data_for_user(event['sender'])
        direct_rooms = chain(*user_account_data.get(AccountDataTypes.DIRECT, {}).values())
        if event.room_id not in direct_rooms:
            # Not a direct message room (of the sender)
            return NOT_SPAM

        try:
            if event.type in self._config.type_blacklist:
                logger.info('RestrictDirectMessages: Blocked message type %r in direct message room %r.', event.type, event.room_id)
                return errors.Codes.FORBIDDEN

        except KeyError:
            pass
        return NOT_SPAM

    async def restrict_direct_message_invite(self, inviter: str, invitee: str, room_id: str) -> Union[NOT_SPAM, errors.Codes]:
        # Retrieve user account data
        user_account_data = await self._api._store.get_global_account_data_for_user(inviter)
        direct_rooms = chain(*user_account_data.get(AccountDataTypes.DIRECT, {}).values())
        if room_id not in direct_rooms:
            # Not a direct message room (of the sender)
            return NOT_SPAM

        # retreive memberships for this room
        room_memberships = await self._api._storage_controllers.state.get_current_state(
                    room_id=room_id, state_filter=membership_state_filer
            )
        # only consider active memberships, not LEAVE, BAN and KNOCK
        room_memberships = {key: value for key, value in room_memberships.items()
                            if value.content.get('membership') in (Membership.INVITE, Membership.JOIN)}

        # with the invite, is the maximum reached?
        if len(room_memberships) + 1 >= self._config.maximum_members:
            logger.error('RestrictDirectMessages: Blocked invite of %r by %r to room %r, because %d members are in the room already, maximum %d.',
                         invitee, invitee, room_id, len(room_memberships), self._config.maximum_members)
            return errors.Codes.FORBIDDEN

        return NOT_SPAM
