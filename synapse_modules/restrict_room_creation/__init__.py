from synapse.module_api import ModuleApi, NOT_SPAM, errors
from synapse.module_api.errors import SynapseError
from synapse.types import Requester, UserID
from synapse.events import EventBase
from synapse.types.state import StateFilter
from synapse.api.constants import EventTypes

from typing import Union, Dict, Any
import attr
import logging
logger = logging.getLogger(__name__)


@attr.s(auto_attribs=True, frozen=True)
class RestrictRoomCreationConfig:
    error_message_top_rooms: str


state_filter = StateFilter.from_types([
    (EventTypes.SpaceParent, None),
    (EventTypes.JoinRules, ""),
])


class RestrictRoomCreation:
    def __init__(self, config: RestrictRoomCreationConfig, api: ModuleApi):
        # Keep a reference to the config and Module API
        self._api = api
        self._config = config

        self._api.register_third_party_rules_callbacks(
            on_create_room=self.on_create_room_callback,
        )

    @staticmethod
    def parse_config(config: Dict[str, Any]) -> RestrictRoomCreationConfig:
        error_message_top_rooms = config.get('error_message_top_rooms', 'Only server admins can create new spaces and rooms in Home.')
        return RestrictRoomCreationConfig(error_message_top_rooms=error_message_top_rooms)

    async def on_create_room_callback(self,
                                      requester: Requester,
                                      request_content: dict,
                                      is_requester_admin: bool) -> None:
        parent = None
        is_direct = request_content.get("is_direct")
        for state in request_content.get("initial_state", []):
            if state.get("type") == "m.space.parent":
                parent = state.get("state_key")
        logger.info('RestrictRoomCreation: Received request from requester %r. Detected parent: %r. Is_direct: %r. Initial state: %r.',
                    requester.user, parent, is_direct, request_content.get("initial_state", []))
        # Restrict top room/space creation to server admins
        if not is_direct and parent is None and not is_requester_admin:
            logger.error('RestrictRoomCreation: Only server admins can create new spaces and rooms in Home.')
            raise SynapseError(
                403, self._config.error_message_top_rooms
            )
        logger.debug('RestrictRoomCreation: Allowing creation of %r by %r.',
                     request_content.get('name', '[no name]'), requester.user)


@attr.s(auto_attribs=True, frozen=True)
class RestrictJoinRulesConfig:
    pass


class RestrictJoinRules:
    def __init__(self, config: dict, api: ModuleApi):
        # Keep a reference to the config and Module API
        self._api = api
        self._config = config

        self._api.register_spam_checker_callbacks(
            check_event_for_spam=self.check_join_rules_callback,
        )

    @staticmethod
    def parse_config(config: Dict[str, Any]) -> RestrictJoinRulesConfig:
        return RestrictJoinRulesConfig()

    async def check_join_rules_callback(self, event: EventBase) -> Union[NOT_SPAM, errors.Codes]:
        if event['type'] != 'm.room.join_rules':
            return NOT_SPAM
        # If the room has no space, only server admins are allowed to change the join rules
        # This intentionally also applies to direct message rooms, otherwise they can be made public
        # For rooms in space we don't need to apply any additional restriction
        # Room creations must be allowed, therefore we check if a join rule did exist before. If not, the room is new and the event is allowed
        is_requester_admin = await self._api._store.is_server_admin(UserID.from_string(event['sender']))
        room_state = await self._api._storage_controllers.state.get_current_state(
            room_id=event.room_id, state_filter=state_filter
        )
        has_parent = False
        for state_event in room_state.values():
            if state_event.type == EventTypes.SpaceParent:
                has_parent = True
            if state_event.type == EventTypes.JoinRules:
                has_join_rule = True

        if has_join_rule and not has_parent and not is_requester_admin:
            logger.error("RestrictJoinRules: Denying join rules event: Join rules of rooms without parent can only be changed by server admins.")
            return errors.Codes.FORBIDDEN
        return NOT_SPAM
