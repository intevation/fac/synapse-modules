from synapse.module_api import ModuleApi
from synapse.api.constants import RoomTypes

from typing import Dict, Any

import logging
logger = logging.getLogger(__name__)


class PublicSpacesPublished:
    def __init__(self, config: Dict, api: ModuleApi):
        # Keep a reference to the config and Module API
        self._api = api
        self._config = config

        self._api.register_third_party_rules_callbacks(
            on_create_room=self.public_spaces_published_callback,
        )

    @staticmethod
    def parse_config(config: Dict[str, Any]) -> Dict:
        return Dict

    async def public_spaces_published_callback(self,
                                              requester: "synapse.types.Requester",
                                              request_content: dict,
                                              is_requester_admin: bool,
                                              ) -> None:
        logger.info('PublicSpacesPublished: Received request: %r', request_content)
        # only spaces have request_content['creation_content']['type'] == RoomTypes.SPACE
        # Rooms don't have creation_content at all
        if request_content.get('creation_content', {}).get('type') != RoomTypes.SPACE:
            logger.info('PublicSpacesPublished: %r created by %r is not a space, skipping.',
                        request_content.get('name', '[no name]'), requester.user)
            return
        visibility = request_content.get('visibility', 'private')
        if visibility == 'private':
            logger.info('PublicSpacesPublished: Enforcing public visibility on space %r '
                        'created by %r.', request_content.get('name', '[no name]'), requester.user)
            request_content['visibility'] = 'public'
