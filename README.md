# Synapse Modules for FEZ Chat

Each of the modules can be used separately and can be added to any [Matrix](https://matrix.org/) [Synapse Server](https://matrix-org.github.io/synapse).
For general documentation on modules in Synapse, read [the official documentation](https://matrix-org.github.io/synapse/latest/modules/index.html).

## Message Type Filter

This module blocks Matrix message types (`event.content['msgtype']`) on the server.

All users will be unable to submit them to the server.

Example configuration:
```yaml
  - module: synapse_modules.message_type_filter.MessageTypeFilter
    config:
      blacklist:
        - m.location
        - m.audio
```

The module logs the rejection of messages with log level INFO.

## Restrict Direct Messages

This module can

* block event types `event.type` in direct messages, preventing the customization of direct messages from feeling like rooms and mimicking private rooms (parameter `type_blacklist`)
* limit the maximum number of members in direct message rooms (parameter `maximum_members`). The membership states *join* and *invite* are considered active *memberships*.

Both parameters are optional.

```yaml
  - module: synapse_modules.restrict_direct_messages.RestrictDirectMessages
    config:
      type_blacklist:
        - m.room.name
        - m.room.topic
        - m.room.avatar
      maximum_members: 2
```



The modules logs

* rejected message types and the affected room ID on log level INFO
* blocked invites to rooms, logging inviter, invitee, room ID and the number of memberships on logging level ERROR.

## Restrict Room Creation

This module restricts the creation of rooms and spaces without direct space parent to synapse admins.
Regular users won't be able to create spaces and rooms freely. Only in spaces if they have the explicit permissions, with the regular permission system, to do so.

The module also bans changing existing rooms' join rules to prevent the rules' circumvention.
Room admins are not allowed to make a private room public if this room has no space parent.
This restriction also applies to direct message rooms (otherwise, they can be made public rooms as well).

No configuration is required.
`error_message_top_rooms` is an optional configuration parameter. This is the error message shown to users when they try to create forbidden rooms/spaces. No localization is possible here, so you may choose the language of the primary target group for this string.

```yaml
  - module: synapse_modules.restrict_room_creation.RestrictRoomCreation
    config:
      error_message_top_rooms: Only server admins can create new spaces and rooms in Home.
  - module: synapse_modules.restrict_room_creation.RestrictJoinRules
```

## Static Account Data

This module keeps some account data static.

* On account creation, the data is added to the account
* if the user changes their account data, the account data is checked for correctness and preserved

An example configuration (for the Maunium Stickerpicker):

```yaml
  - module: synapse_modules.static_account_data.StaticAccountData
    config:
      - data_type: m.widgets
        data: {"stickerpicker":{"content":{"type":"m.stickerpicker","url":"https://maunium.example.com/?theme=$theme","name":"Stickerpicker","data":{}},"state_key":"stickerpicker","type":"m.widget","id":"stickerpicker"}}
```

## Top Spaces

Provides a new API resource `/_synapse/client/topUnjoinedSpaces` which lists all space rooms which have no parent (top spaces) and the user has not joined:
The API call must be authenticated.

No configuration is needed:

```yaml
  - module: synapse_modules.top_spaces.TopSpaces
    config:
```

The response looks like this:

```bash
> curl -H 'authorization: Bearer $token' https://matrix.example.com/_synapse/client/topUnjoinedSpaces | jq .
{
  "chunk": [
    {
      "room_id": "!znOpvbLrUzahxUUGUY:matrix.example.com",
      "name": "Test 1",
      "topic": "Topic of Test 1",
      "canonical_alias": "#test1:matrix.example.com",
      "num_joined_members": 1,
      "world_readable": true,
      "guest_can_join": true,
      "join_rule": "public",
      "room_type": "m.space"
    },
    {
      "room_id": "!vzZmioYiutyGpOFury:matrix.example.com",
      "name": "Test Space",
      "topic": "",
      "canonical_alias": "#test-space:matrix.example.com",
      "num_joined_members": 1,
      "world_readable": true,
      "guest_can_join": true,
      "join_rule": "public",
      "room_type": "m.space"
    },
  ],
  "total_room_count_estimate": 2
}
```

## Public Spaces Published

This module ensures that public spaces are created with public visibility, i.e. are published in the room directory.
This is useful if the visibility setting is not visible in the UI for simplicity.
Additionally, public spaces create with th mobile clients are by default not published in the room directory, as opposed to the web client.

No settings are required for this module.
The module logs, if it changed a room creation event, the room name and creator.

```yaml
  - module: synapse_modules.public_spaces_published.PublicSpacesPublished
```
